# Supportan #

Support management application.

### Build

```shell
    make build
```

Or you can use IDE runner for gradle tasks, then you only need to run:

```shell
    make build-app-image
```

### Run

```shell
    make up
```

### Postman

https://www.getpostman.com/collections/a3a8998aa6387117dbb5
