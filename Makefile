APP_IMAGE=pm.dvl.supportan/app
VERSION=latest
PWD=$(shell pwd)

.PHONY: build

dc = docker-compose
upCommand = $(dc) up
downCommand = $(dc) down

#-------------------------------------
# Build
#-------------------------------------
build: build-app-image build-app

build-app-image:
	docker build ./docker/app -t $(APP_IMAGE):$(VERSION) --no-cache

build-app:
	docker run -v $(PWD):/app -w /app gradle:jdk15 gradle build --no-daemon -S -d

#-------------------------------------
# Run
#-------------------------------------
up:
	$(upCommand) -d

ups:
	$(upCommand)

down:
	$(downCommand)

restart:
	$(dc) restart app
