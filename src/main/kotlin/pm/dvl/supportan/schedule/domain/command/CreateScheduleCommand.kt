package pm.dvl.supportan.schedule.domain.command

import org.axonframework.modelling.command.TargetAggregateIdentifier
import pm.dvl.supportan.role.domain.model.RoleName
import pm.dvl.supportan.schedule.domain.model.*

data class CreateScheduleCommand(
    @TargetAggregateIdentifier
    val name: ScheduleName,
    val roleName: RoleName,
    val period: Period,
    val workingTime: WorkingTime,
    val rotation: Rotation,
    val executors: Executors,
    val priority: Int = 0
)
