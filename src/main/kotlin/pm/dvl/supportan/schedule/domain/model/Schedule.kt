package pm.dvl.supportan.schedule.domain.model

import org.axonframework.commandhandling.CommandHandler
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.spring.stereotype.Aggregate
import pm.dvl.supportan.role.domain.model.RoleName
import pm.dvl.supportan.schedule.domain.command.CreateScheduleCommand
import org.axonframework.modelling.command.AggregateLifecycle.apply
import pm.dvl.supportan.schedule.domain.event.ScheduleCreatedEvent

@Aggregate
class Schedule() {
    @AggregateIdentifier
    private lateinit var name: ScheduleName
    private lateinit var roleName: RoleName
    private lateinit var period: Period
    private lateinit var workingTime: WorkingTime
    private lateinit var rotation: Rotation
    private lateinit var executors: Executors
    private var priority: Int = 0

    @CommandHandler
    constructor(command: CreateScheduleCommand) : this() {
        apply(command.toScheduleCreatedEvent())
    }

    private final fun CreateScheduleCommand.toScheduleCreatedEvent(): ScheduleCreatedEvent {
        return ScheduleCreatedEvent(name, roleName, period, workingTime, rotation, executors, priority)
    }

    @EventSourcingHandler
    fun on(event: ScheduleCreatedEvent) {
        name = event.name
        roleName = event.roleName
        period = event.period
        workingTime = event.workingTime
        rotation = event.rotation
        executors = event.executors
        priority = event.priority
    }
}
