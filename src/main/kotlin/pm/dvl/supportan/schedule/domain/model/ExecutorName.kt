package pm.dvl.supportan.schedule.domain.model

data class ExecutorName(val value: String)
