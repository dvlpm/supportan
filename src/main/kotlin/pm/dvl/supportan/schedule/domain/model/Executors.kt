package pm.dvl.supportan.schedule.domain.model

class Executors(vararg executors: Executor) : MutableSet<Executor> by mutableSetOf(*executors)
