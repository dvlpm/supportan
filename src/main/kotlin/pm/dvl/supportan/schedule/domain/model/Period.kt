package pm.dvl.supportan.schedule.domain.model

import pm.dvl.supportan.common.domain.model.DateRanges

class Period(
    val include: DateRanges,
    val exclude: DateRanges
)
