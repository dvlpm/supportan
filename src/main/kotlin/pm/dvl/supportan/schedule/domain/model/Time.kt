package pm.dvl.supportan.schedule.domain.model

import java.time.LocalTime

class Time(val value: LocalTime)
