package pm.dvl.supportan.schedule.domain.model

data class WorkingTime(
    val from: Time,
    val to: Time,
)
