package pm.dvl.supportan.schedule.domain.model

import java.time.DayOfWeek

data class Rotation(
    val every: Int,
    val workdays: Set<DayOfWeek>
)
