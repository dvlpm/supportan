package pm.dvl.supportan.schedule.domain.model

data class ScheduleName(val value: String)
