package pm.dvl.supportan.schedule.domain.model

import pm.dvl.supportan.common.domain.model.DateRanges

class Executor(val name: ExecutorName, val unavailableAt: DateRanges)
