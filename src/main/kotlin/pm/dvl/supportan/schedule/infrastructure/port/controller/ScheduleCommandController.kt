package pm.dvl.supportan.schedule.infrastructure.port.controller

import org.axonframework.commandhandling.gateway.CommandGateway
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import pm.dvl.supportan.schedule.domain.command.CreateScheduleCommand
import pm.dvl.supportan.schedule.domain.model.*
import pm.dvl.supportan.schedule.infrastructure.port.request.CreateScheduleRequest
import pm.dvl.supportan.schedule.infrastructure.port.response.CreateScheduleResponse

@RestController
class ScheduleCommandController(val commandGateway: CommandGateway) {
    @PostMapping("/schedules")
    fun create(@RequestBody request: CreateScheduleRequest): CreateScheduleResponse {
        val name = commandGateway.sendAndWait<ScheduleName>(request.toCreateScheduleCommand())

        return CreateScheduleResponse(name)
    }

    fun CreateScheduleRequest.toCreateScheduleCommand(): CreateScheduleCommand {
        return CreateScheduleCommand(name, roleName, period, workingTime, rotation, executors, priority)
    }
}
