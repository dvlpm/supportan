package pm.dvl.supportan.schedule.infrastructure.port.request

import pm.dvl.supportan.role.domain.model.RoleName
import pm.dvl.supportan.schedule.domain.model.*

data class CreateScheduleRequest(
    val name: ScheduleName,
    val roleName: RoleName,
    val period: Period,
    val workingTime: WorkingTime,
    val rotation: Rotation,
    val executors: Executors,
    val priority: Int = 0
)
