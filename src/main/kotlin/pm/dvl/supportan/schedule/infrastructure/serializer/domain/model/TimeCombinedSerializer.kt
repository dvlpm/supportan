package pm.dvl.supportan.schedule.infrastructure.serializer.domain.model

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import org.springframework.boot.jackson.JsonComponent
import pm.dvl.supportan.schedule.domain.model.Time
import java.time.LocalTime

@JsonComponent
class TimeCombinedSerializer {
    class Deserializer : JsonDeserializer<Time>() {
        override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): Time {
            return p?.valueAsString.toTime()
        }

        private fun String?.toTime(): Time {
            return Time(LocalTime.parse((this ?: "")))
        }
    }

    class Serializer : JsonSerializer<Time>() {
        override fun serialize(value: Time?, gen: JsonGenerator?, serializers: SerializerProvider?) {
            gen?.writeString(value?.value.toString())
        }
    }
}


