package pm.dvl.supportan.schedule.infrastructure.serializer.domain.model

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import org.springframework.boot.jackson.JsonComponent
import java.time.DayOfWeek

@JsonComponent
class DayOfWeekCombinedSerializer {
    class Deserializer : JsonDeserializer<DayOfWeek>() {
        override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): DayOfWeek {
            return DayOfWeek.valueOf(p?.valueAsString?.toUpperCase() ?: "")
        }
    }

    class Serializer : JsonSerializer<DayOfWeek>() {
        override fun serialize(value: DayOfWeek?, gen: JsonGenerator?, serializers: SerializerProvider?) {
            gen?.writeString(value?.toString())
        }
    }
}
