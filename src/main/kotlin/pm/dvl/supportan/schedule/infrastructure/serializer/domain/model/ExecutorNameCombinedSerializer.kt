package pm.dvl.supportan.schedule.infrastructure.serializer.domain.model

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import org.springframework.boot.jackson.JsonComponent
import pm.dvl.supportan.schedule.domain.model.ExecutorName

@JsonComponent
class ExecutorNameCombinedSerializer {
    class Deserializer : JsonDeserializer<ExecutorName>() {
        override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): ExecutorName {
            return ExecutorName(p?.valueAsString ?: "")
        }
    }

    class Serializer : JsonSerializer<ExecutorName>() {
        override fun serialize(value: ExecutorName?, gen: JsonGenerator?, serializers: SerializerProvider?) {
            gen?.writeString(value?.value)
        }
    }
}
