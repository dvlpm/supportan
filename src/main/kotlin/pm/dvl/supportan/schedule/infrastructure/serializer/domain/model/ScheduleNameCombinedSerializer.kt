package pm.dvl.supportan.schedule.infrastructure.serializer.domain.model

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import org.springframework.boot.jackson.JsonComponent
import pm.dvl.supportan.schedule.domain.model.ScheduleName

@JsonComponent
class ScheduleNameCombinedSerializer {
    class Deserializer : JsonDeserializer<ScheduleName>() {
        override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): ScheduleName {
            return ScheduleName(p?.valueAsString ?: "")
        }
    }

    class Serializer : JsonSerializer<ScheduleName>() {
        override fun serialize(value: ScheduleName?, gen: JsonGenerator?, serializers: SerializerProvider?) {
            gen?.writeString(value?.value)
        }
    }
}
