package pm.dvl.supportan.schedule.infrastructure.port.response

import pm.dvl.supportan.schedule.domain.model.ScheduleName

data class CreateScheduleResponse(val name: ScheduleName)
