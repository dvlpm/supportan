package pm.dvl.supportan.schedule.infrastructure.serializer.domain.model

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import org.springframework.boot.jackson.JsonComponent
import pm.dvl.supportan.role.domain.model.RoleName

@JsonComponent
class RoleNameCombinedSerializer {
    class Deserializer : JsonDeserializer<RoleName>() {
        override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): RoleName {
            return RoleName(p?.valueAsString ?: "")
        }
    }

    class Serializer : JsonSerializer<RoleName>() {
        override fun serialize(value: RoleName?, gen: JsonGenerator?, serializers: SerializerProvider?) {
            gen?.writeString(value?.value)
        }
    }
}
