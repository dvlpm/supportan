package pm.dvl.supportan.shift.domain.model

import pm.dvl.supportan.common.domain.model.DateRange
import pm.dvl.supportan.role.domain.model.RoleName
import pm.dvl.supportan.schedule.domain.model.ExecutorName
import pm.dvl.supportan.schedule.domain.model.ScheduleName

class Shift() {
    private lateinit var roleName: RoleName
    private lateinit var scheduleName: ScheduleName
    private lateinit var executorName: ExecutorName
    private lateinit var interval: DateRange
}
