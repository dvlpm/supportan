package pm.dvl.supportan.role.domain.model

data class RoleName(val value: String)
