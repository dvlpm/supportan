package pm.dvl.supportan.common.domain.model

import java.util.*

data class DateRange(
    val from: Date,
    val to: Date
)
