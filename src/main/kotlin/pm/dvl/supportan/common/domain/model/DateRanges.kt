package pm.dvl.supportan.common.domain.model

import pm.dvl.supportan.common.domain.model.DateRange

class DateRanges(vararg dateRanges: DateRange) : MutableSet<DateRange> by mutableSetOf(*dateRanges)
