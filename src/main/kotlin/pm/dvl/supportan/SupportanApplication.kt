package pm.dvl.supportan

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SupportanApplication

fun main(args: Array<String>) {
	runApplication<SupportanApplication>(*args)
}
