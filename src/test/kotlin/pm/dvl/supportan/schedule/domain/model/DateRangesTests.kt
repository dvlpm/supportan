package pm.dvl.supportan.schedule.domain.model

import org.junit.jupiter.api.Assertions.assertEquals

import org.junit.jupiter.api.Test
import pm.dvl.supportan.common.domain.model.DateRange
import pm.dvl.supportan.common.domain.model.DateRanges
import java.util.*

class DateRangesTests {
    @Test
    fun `create ranges`() {
        val firstRange = DateRange(Date(), Date())
        val secondRange = DateRange(Date(), Date())
        val ranges = DateRanges(firstRange, secondRange)

        assertEquals(2, ranges.size)
        assertEquals(firstRange, ranges.elementAt(0))
        assertEquals(secondRange, ranges.elementAt(1))
    }
}
